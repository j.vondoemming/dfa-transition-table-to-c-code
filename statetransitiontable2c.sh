#!/bin/bash

function printUsage() {
	echo "Usage: $0 <INFILE> <OUTFILE> [PREFIX] [SUFFIX]"
	echo "	Using stdin  if INFILE  is '-'."
	echo "	Using stdout if OUTFILE is '-'."
	exit 1
}


if [ $# -le 1 ]; then
	printUsage
fi
if [ $# -gt 4 ]; then
	printUsage
fi

INP=""
if [ "$1" = "-" ]; then
	INP="$(cat -)"
else
	INP="$(cat "$1")"
fi

if [ "$2" != "-" ]; then
	exec 1> $2
fi

PREFIX=""
SUFFIX=""
if [ $# -ge 3 ]; then
	PREFIX="$3"
fi
if [ $# -ge 4 ]; then
	SUFFIX="$4"
fi



ALPHABET="$(echo "$INP" | head -n 1 | sed 's,^δ	,,')"
TABLE="$(echo "$INP" | tail -n +2)"
STATES="$(echo "$TABLE" | cut -f 1)"
TABLEWOCURSTATE="$(echo "$TABLE" | cut -f '2-')"

NUMSTATES="$(echo "$STATES" | wc -l)"
NUMALPHABET="$(echo "$ALPHABET" | tr '\t' '\n' | wc -l)"

#echo "INP: \"$INP\""
#echo "ALPHABET[${NUMALPHABET}]: \"$ALPHABET\""
#echo "TABLE: \"$TABLE\""
#echo "TABLEWOCURSTATE: \"$TABLEWOCURSTATE\""
#echo "STATES[${NUMSTATES}]: \"$STATES\""

echo "enum ${PREFIX}alphabet${SUFFIX} {"
FIRST="1"
echo "$ALPHABET" | tr '\t' '\n' | while read line ; do
	if [ "$FIRST" = "1" ]; then
		FIRST="0"
	else
		printf ","
		printf "\n"
	fi
	printf "	%s" "${PREFIX}alphabet_${line}${SUFFIX}"
done
printf "\n"
echo "};"

echo "enum ${PREFIX}state${SUFFIX} {"
FIRST="1"
echo "$STATES" | while read line ; do
	if [ "$FIRST" = "1" ]; then
		FIRST="0"
	else
		printf ","
		printf "\n"
	fi
	printf "	%s" "${PREFIX}state_${line}${SUFFIX}"
done
printf "\n"
echo "};"

echo "static enum ${PREFIX}state${SUFFIX} ${PREFIX}transition${SUFFIX}(enum ${PREFIX}state${SUFFIX} previousstate, enum ${PREFIX}alphabet${SUFFIX} input) {"

echo "	/* This code was generated using [dfa-transition-table-to-c-code](https://gitlab.gwdg.de/j.vondoemming/dfa-transition-table-to-c-code).*/"
echo "	/* original input: "
echo "$INP" | sed 's,^,	* ,g'
echo "	*/"



echo "	const static enum ${PREFIX}state${SUFFIX} statetransitiontable[${NUMSTATES}][${NUMALPHABET}] = {"
FIRST="1"
echo "$TABLE" | while read line ; do
	CURSTATE="$(echo "$line" | cut -f '1')"
	CURROWWOSTATE="$(echo "$line" | cut -f '2-')"
	if [ "$FIRST" = "1" ]; then
		FIRST="0"
	else
		printf ","
		printf "\n"
	fi
	printf "		/* %s: */ { " "$CURSTATE" 
	echo "$CURROWWOSTATE" | sed 's/\t/,\t/g' | tr '\t' '\n' | while read curoutstate ; do
		printf "%s " "${PREFIX}state_${curoutstate}${SUFFIX}"
	done
	printf "}"
done
printf "\n"
echo "	};"

echo ""
echo "	enum ${PREFIX}state${SUFFIX} res = statetransitiontable[previousstate][input];"
echo ""
echo "	if (res != previousstate) {"
echo "		/* you may add code for transition state changes here */"
echo "	}"
echo "	return res;"
echo "}"

exit 0
